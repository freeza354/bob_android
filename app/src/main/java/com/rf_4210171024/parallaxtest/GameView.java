package com.rf_4210171024.parallaxtest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class GameView extends SurfaceView implements Runnable {

    Thread gameThread = null;
    SurfaceHolder ourHolder;
    volatile boolean playing;

    Canvas canvas;
    Paint paint;

    long fps;
    private long timeThisFrame;

    Bitmap bitmapBob;
    boolean isMoving = false;
    float walkSpeedPerSecond = 150;
    float bobXpos = 10;

    int width;

    public GameView(Context context){
        super(context);

        ourHolder = getHolder();
        paint = new Paint();

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        width = display.getWidth();

        bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.bob);
    }

    @Override
    public void run(){
        while (playing){
            long startFrameTime = System.currentTimeMillis();

            update();
            draw();

            timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame > 0)
                fps = 1000 / timeThisFrame;
        }
    }

    public void update(){
        if (isMoving){

            bobXpos = bobXpos + (walkSpeedPerSecond / fps);

            if (bobXpos >= width || bobXpos <= 0) {
                walkSpeedPerSecond = -walkSpeedPerSecond;
            }

        }
    }

    public void draw(){
        if (ourHolder.getSurface().isValid()){

            canvas = ourHolder.lockCanvas();
            canvas.drawColor(Color.argb(255, 26, 128, 182));
            paint.setColor(Color.argb(255,249,129,0));
            paint.setTextSize(45);

            canvas.drawText("FPS : " + fps, 20, 40, paint);
            canvas.drawBitmap(bitmapBob, bobXpos, 200, paint);
            ourHolder.unlockCanvasAndPost(canvas);

        }
    }

    public void pause(){
        playing = false;
        try {
            gameThread.join();
        }catch (InterruptedException e){
            Log.e("Error : ", "Joining thread.");
        }
    }

    public void resume(){
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent){
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK){

            case MotionEvent.ACTION_DOWN:
                isMoving = true;
                break;
            case MotionEvent.ACTION_UP:
                isMoving = false;
                break;

        }
        return true;
    }

}
